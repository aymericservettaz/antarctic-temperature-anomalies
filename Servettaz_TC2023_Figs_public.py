"""
Temperature Signature of Precipitation Events in Antarctica
File last updated by Aymeric Servettaz on 2023-10-02
Python version 3.9.7

DISCLAIMER:
    
Permission is hereby granted, free of charge, to any person obtaining a copy of 
this software and associated documentation files (the “Software”), to deal in 
the Software without restriction, including without limitation the rights to 
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
of the Software, and to permit persons to whom the Software is furnished to do 
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all 
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE 
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
SOFTWARE.
"""
# cd C:\Python\LSCE_WE\MAR

import xarray as xr
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.ticker as tck
import pickle
import matplotlib.colors as colors
from scipy import odr
from scipy.stats import t
import matplotlib.cm as cm
from scipy import stats

# =============================================================================
#%% functions used
# =============================================================================

def w_mean(x, w):
    m = np.sum(x*w)/np.sum(w)
    return m

def w_cov(x, y, w):
    c = np.sum(w*(x - w_mean(x, w))*(y - w_mean(y, w))) / np.sum(w)
    return c

def w_corr(x, y, w):
    cor = w_cov(x, y, w) / np.sqrt(w_cov(x, x, w)*w_cov(y, y, w))
    return cor

def f(B, x):
    return B[0]*x + B[1]


def do_odr_fit(x, y, w= None, precision = 1, clr='k'):
    nonan=np.logical_and(np.invert(np.isnan(x)), np.invert(np.isnan(y)))
    #Orthogonal LMS on weighted data
    xst = np.array((x- x.mean()) / x.std())
    yst = np.array((y -y.mean())/ y.std())
    
    xr = np.array([x.min(), x.max()])
    if w is not None:
        wst = np.array(w)
        dreg = odr.Data(xst[nonan], yst[nonan], we=wst, wd=wst)
    else:
        dreg = odr.Data(xst[nonan], yst[nonan])
        w = np.ones(len(x))
        wst = np.array(w)
        
    ort_fit = odr.ODR(dreg, odr.Model(f), beta0=[0., 0.])
    myoutput = ort_fit.run()
    slope_odr, intercept_odr  = myoutput.beta
    y0 = y.std()*(slope_odr*(0.- x.mean()) / x.std() + intercept_odr)+y.mean()
    y1 = y.std()*(slope_odr*(1.- x.mean()) / x.std() + intercept_odr)+y.mean()
    plt.plot(xr,xr*(y1-y0) + y0, color=clr, lw=1, zorder=20, label='orthogonal distance least square regression')

    #computation of non-weighted score
    y_true = np.array(yst)
    y_fit  = np.array(xst*slope_odr+intercept_odr)
    r2_score_weighted_w = w_corr(y_true, y_fit, np.array(wst))**2
    n = len(y_true)
    alpha = 0.05
    se_slope = np.sqrt( (1./(n-2.)) * np.sum(wst*(y_true - y_fit)**2) / np.sum(wst*(xst - w_mean(xst, wst))**2))

    t_an = t.ppf(1. - alpha/2., n-2)
    se_err95 = t_an * se_slope
    
    y1_low = y.std()*((slope_odr - se_err95)*(1.- x.mean()) / x.std() + intercept_odr)+y.mean()
    y1_high = y.std()*((slope_odr + se_err95)*(1.- x.mean()) / x.std() + intercept_odr)+y.mean()
    y0_low = y.std()*((slope_odr - se_err95)*(0.- x.mean()) / x.std() + intercept_odr)+y.mean()
    y0_high = y.std()*((slope_odr + se_err95)*(0.- x.mean()) / x.std() + intercept_odr)+y.mean()
    
    slope_min = y1_low-y0_low
    slope_max = y1_high-y0_high
    plt.plot(xr, xr*slope_min + y0_low, color=clr, ls='--', lw=1, zorder=2)
    plt.plot(xr, xr*slope_max + y0_high, color=clr, ls='--', lw=1, zorder=3)
    
    print(str(np.round(y1_low-y0_low, precision +1))+u' < slope < '+ str(np.round(y1_high-y0_high, precision +1)))

    str1 = (u'y = '+str(np.round(y1-y0, precision +1 ))+' * x + '+str(np.round(y0, precision)))
    str2 = (u'r² = '+str(np.round(r2_score_weighted_w, precision +2)))
    
    return str1, str2

def do_odr_corr(x, y, w= None, precision = 1, clr='k'):
    nonan=np.logical_and(np.invert(np.isnan(x)), np.invert(np.isnan(y)))
    #Orthogonal LMS on weighted data
    xst = np.array((x- x.mean()) / x.std())
    yst = np.array((y -y.mean())/ y.std())
    
    xr = np.array([x.min(), x.max()])
    if w is not None:
        wst = np.array(w)
        dreg = odr.Data(xst[nonan], yst[nonan], we=wst, wd=wst)
    else:
        dreg = odr.Data(xst[nonan], yst[nonan])
        w = np.ones(len(x))
        wst = np.array(w)
        
    ort_fit = odr.ODR(dreg, odr.Model(f), beta0=[0., 0.])
    myoutput = ort_fit.run()
    slope_odr, intercept_odr  = myoutput.beta
    y0 = y.std()*(slope_odr*(0.- x.mean()) / x.std() + intercept_odr)+y.mean()
    y1 = y.std()*(slope_odr*(1.- x.mean()) / x.std() + intercept_odr)+y.mean()
    plt.plot(xr,xr*(y1-y0) + y0, color=clr, lw=1, zorder=20, label='orthogonal distance least square regression')

    #computation of non-weighted score
    y_true = np.array(yst)
    y_fit  = np.array(xst*slope_odr+intercept_odr)
    # r2_score_weighted_w = w_corr(y_true, y_fit, np.array(wst))**2
    n = len(y_true)
    alpha = 0.05
    se_slope = np.sqrt( (1./(n-2.)) * np.sum(wst*(y_true - y_fit)**2) / np.sum(wst*(xst - w_mean(xst, wst))**2))

    t_an = t.ppf(1. - alpha/2., n-2)
    se_err95 = t_an * se_slope
    
    y1_low = y.std()*((slope_odr - se_err95)*(1.- x.mean()) / x.std() + intercept_odr)+y.mean()
    y1_high = y.std()*((slope_odr + se_err95)*(1.- x.mean()) / x.std() + intercept_odr)+y.mean()
    y0_low = y.std()*((slope_odr - se_err95)*(0.- x.mean()) / x.std() + intercept_odr)+y.mean()
    y0_high = y.std()*((slope_odr + se_err95)*(0.- x.mean()) / x.std() + intercept_odr)+y.mean()
    
    slope_min = y1_low-y0_low
    slope_max = y1_high-y0_high
    plt.plot(xr, xr*slope_min + y0_low, color=clr, ls='--', lw=1, zorder=2)
    plt.plot(xr, xr*slope_max + y0_high, color=clr, ls='--', lw=1, zorder=3)
    
    print(str(np.round(y1_low-y0_low, precision +1))+u' < slope < '+ str(np.round(y1_high-y0_high, precision +1)))
    return (y1-y0), y0

colmap = cm.get_cmap('RdBu_r')

def scatter_corr(x, y, c, w=None):
    plt.figure()
    a, b = do_odr_corr(x, y, w, precision=3)
    plt.close(plt.gcf())
    plt.figure(figsize = (3.5, 3.))
    somin, somax = x.min(), x.max()
    vmin, vmax = np.nanmin(c), np.nanmax(c)
    ccc = colmap((c-vmin)/ (vmax-vmin))
    plt.scatter(x, y, marker = '+', c = ccc, alpha = .12)
    plt.plot(np.array([somin, somax]), a*np.array([somin, somax])+ b, color = 'b')
    plt.annotate('y = '+"%.2f" % a+' * x + '+"%.2f" % b, xy=(0.02, 0.95), xycoords='axes fraction')
    # r_sq = np.corrcoef(y, a*so_surface.loc[:, x]+b)[0,1]**2
    r, p_val = stats.pearsonr(y, a*x+b)
    plt.annotate('r$^{2}$ = '+ "%.2f" % r**2, xy=(0.02, 0.88), xycoords='axes fraction')
    if p_val >= 0.01:
        plt.annotate('P$_{value}$ = '+"%.3f" % p_val, xy=(0.02, 0.81), xycoords='axes fraction')
    else:
        plt.annotate('P$_{value}$ < 0.01', xy=(0.02, 0.81), xycoords='axes fraction')

def shade(var2d, vmin, vmax, delta, cmap='viridis', extend='both', label=''):
    xmin, xmax = -2800., 2800.
    ymin, ymax = -2500., 2500.
    levels = np.arange(vmin, vmax+delta, delta)
    plt.figure(figsize=(5.5, 4.))
    plt.clf()
    plt.subplots_adjust(left=0.02, bottom=0.02, top=0.98, right=0.98)
    plt.setp(plt.gca().get_xaxis(), visible=False)#Hide bottom x-axis
    plt.setp(plt.gca().get_yaxis(), visible=False)#Hide y-axis
    plt.contourf(xx, yy, var2d, levels=levels, cmap=cmap, extend=extend)
    plt.colorbar(label=label, pad=0.02)
    plt.xlim(xmin, xmax)
    plt.ylim(ymin, ymax)
    plt.contour(xx, yy, lat2d, np.linspace(-80, -50, 4), colors='k', linewidths=.6, linestyles='dotted')
    plt.contour(xx, yy, lon2d.where((lat2d >= -88) & (lon2d>-179)), np.linspace(-180, 180.*5./6., 12), colors='k', linewidths=.7, linestyles='dotted')
    plt.contour(xx, yy, sh, [0.], linewidths=1.2, colors='k')
    plt.contour(xx, yy, sh, [100., 1000., 2000., 3000., 4000.], linewidths=.6, colors='k')
    coordannot = [u'180°', u'150°W', u'120°W', u'90°W', u'60°W', u'30°W', u'0°', u'30°E', u'60°E', u'90°E', u'120°E', u'150°E']
    for i in range(len(coordannot)):
        plt.annotate(coordannot[i], xy=(xx.isel(x =xcoords[i]),  yy.isel(y=ycoords[i])))
    
def overlay_sites():
    for si in sitemar:
        i=sitemar[si]
        plt.plot(xx.isel(x=i[0]), yy.isel(y=i[1]), marker = 'o', mfc = 'k', ms = 7, mec='none')
# common cmap and levels for fig1

def create_naxis(axes_number, cover=0.):
    """create a figure with n subplots that share a same box
    parameters:
        axes_number (int): number of axes to create
        [optional] cover (float): ratio of shared axis between boxes
                                  values between -1. and 1.
                                  negative values create an empty space between axes
    outputs:
        fbox: axis handle for the contouring box
        axis_list: list of axes corresponding to subplots
    """
    cover=cover/2.
    n=axes_number
    fig=plt.figure()
    fbox=fig.add_subplot(n,1,n)#create a root subplot
    axis_list=['']*n#create a list that will contain all the axes handles
    for i in range(n):#create n subplots that will be rearranged later
        axis_list[i]=fig.add_subplot(n, 1, i+1,sharex=fbox)
    
    x0=axis_list[-1].get_position().x0#Left
    y0=axis_list[-1].get_position().y0#Lower
    x1=axis_list[0].get_position().x1#Right
    y1=axis_list[0].get_position().y1#Upper
    
    from matplotlib.transforms import Bbox
    l=y1-y0#full height = upper-lower
    
    for i in range(n):
        box_bot_ax   = y0 + l*float(n-1-i)/n
        box_top_ax   = y0 + l*float(n-i)/n
        box_allocate = Bbox([[x0, box_bot_ax-cover*l/n],[x1, box_top_ax+cover*l/n]])
        axis_list[i].set_position(box_allocate)
        axis_list[i].spines['top'].set_visible(False)
        axis_list[i].spines['bottom'].set_visible(False)
        plt.setp(axis_list[i].get_xaxis(), visible=False)
        axis_list[i].patch.set_alpha(0.)
        if (i % 2) == 0:
            axis_list[i].yaxis.set_label_position("left")
            axis_list[i].yaxis.tick_left()
        else:
            axis_list[i].yaxis.set_label_position("right")
            axis_list[i].yaxis.tick_right()
    
    box1= Bbox([[x0, y0 + l*float(n-1)/n - 2*cover*l/n],[x1,y1]])
    boxn= Bbox([[x0,y0],[x1, y0 + l*float(1)/n + 2*cover*l/n]])
    boxfb= Bbox([[x0,y0],[x1,y1]])
    
    
    axis_list[0].set_position(box1)#redefine top limit of first box
    axis_list[0].spines['top'].set_visible(True)
    
    axis_list[-1].set_position(boxn)#redefine bot limit of last box
    axis_list[-1].spines['bottom'].set_visible(True)
    plt.setp(axis_list[-1].get_xaxis(), visible=True)#show bottom x-axis
    
    fbox.set_position(boxfb)#enlarge fullbox to all plots
    plt.setp(fbox.get_xaxis(), visible=False)
    plt.setp(fbox.get_yaxis(), visible=False)
    fbox.patch.set_alpha(1.)#it acts as the background: all other alpha layers are transparent
    return fbox, axis_list

def to_bindim(x, y, nbins=80):
    #x is log10(1+SF), not SF
    binx = np.interp(x, np.log10(plim), [0, nbins], left = np.nan, right = np.nan)
    biny = np.interp(y, blim, [0, nbins], left = np.nan, right = np.nan)
    return binx, biny

def replot_encart_scatter_density_dta(h, region_name, plim, blim, vvmax = 5e2):
    # if vvmax == 'auto':
    #     vvmax = 0.5*np.max(h)
    nbins = np.shape(h)[0]
    plt.figure(figsize = (3.5, 3.))
    # plt.imshow(h, norm=colors.LogNorm(vmin=1, vmax=np.max(h)), cmap='YlGnBu')#Blues, YlGnBu, GnBu
    plt.imshow(h, norm=colors.LogNorm(vmin=1, vmax=vvmax), cmap='YlGnBu')#Blues, YlGnBu, GnBu
    # plt.colorbar(label='day count per pixel')
    
    # Set Major Ticks
    logxtval = np.arange(np.log10(plim[0]), np.log10(plim[1]), 1.)
    text_xticks = np.hstack([0, 10.**logxtval, plim[1]-1])
    target_xticks = np.log10(text_xticks+1)
    tickloc_bindim = np.interp(target_xticks, [np.log10(plim[0]), np.log10(plim[1])], [0, np.shape(h)[0]])
    # plt.gca().set_xticks(np.log10(target_xticks+1))
    plt.gca().set_xticks(tickloc_bindim)
    plt.gca().set_xticklabels(text_xticks.astype(int).astype(str))
    # plt.xlabel('Daily Snowfall (kg m$^{-2}$ d$^{-1}$)')
    plt.xlabel('SF (kg m$^{-2}$ d$^{-1}$)')
    # plt.ylabel(u'Temperature Anomaly (°C)')
    plt.ylabel(u'T\' (°C)', rotation = 0)
    ytickname = np.arange(-20., 36., 10.)
    ytickloc = np.interp(ytickname, blim, [0, nbins])
    plt.gca().set_yticks(ytickloc)
    plt.gca().set_yticklabels(ytickname.astype(str))
    plt.gca().invert_yaxis()
    # plt.annotate(region_name, xy = (nbins-2, nbins-2), fontsize=16, ha = 'right')
    plt.annotate(region_name, xy = (10, nbins-2), fontsize=16, ha = 'left')
    
    #  Set minorticks
    logxtval2 = np.arange(0., 1.1, 1.)
    temp = np.vstack([2*10.**logxtval2, 3*10.**logxtval2, 4*10.**logxtval2, 5*10.**logxtval2, 
                      6*10.**logxtval2, 7*10.**logxtval2, 8*10.**logxtval2, 9*10.**logxtval2])
    tt=(np.sort(np.reshape(temp, (np.size(temp),)))+1)
    target_minxticks = np.log10(tt[tt<plim[1]])
    mintickloc_bindim = np.interp(target_minxticks, [np.log10(plim[0]), np.log10(plim[1])], [0, np.shape(h)[0]])
    plt.gca().xaxis.set_minor_locator(tck.FixedLocator(mintickloc_bindim))
    plt.gca().spines['top'].set_visible(False)
    plt.gca().spines['right'].set_visible(False)
    # draw mean
    yhvals = np.interp(np.arange(nbins)+0.5, [0, nbins], blim)
    mean_dta = np.zeros(nbins)
    for sf in range(nbins):
        sfbin_dta = np.multiply(h[:,sf], yhvals)
        mean_dta[sf] = np.sum(sfbin_dta)/np.sum(h[:,sf])
    # plt.plot(np.arange(nbins),np.interp(mean_dta, blim, [0, nbins]), color = (1., .65, .65), lw = .4)
    plt.plot(np.arange(nbins),np.interp(0*np.arange(nbins), blim, [0, nbins]), ls ='--', color = (.05, .05, .05))
    plt.plot(np.arange(nbins),np.interp(mean_dta, blim, [0, nbins]), color = (1., .05, .05))
    # #  plot trends
    slope = trends_per_site[region_name][0]
    y0 = trends_per_site[region_name][1]
    ytrend = y0 + slope*xdx
    ax1, ay2 = to_bindim(xdx, ytrend, nbins)
    plt.plot(ax1, ay2, ls = ':',color = (.05, .05, .05))
    if y0 < 0.:
        plt.annotate('T\'='+str(slope)+'*log$_{10}$(1+SF)-'+str(-y0), xy = (nbins-.1, nbins/2.-22), fontsize=11, ha = 'right')
    else:
        plt.annotate('T\'='+str(slope)+'*log$_{10}$(1+SF)+'+str(y0), xy = (nbins-.1, nbins/2.-22), fontsize=11, ha = 'right')
    plt.annotate('r$^{2}$ = '+str(trends_per_site[region_name][2]), xy = (nbins-.1, nbins/2.-30), fontsize=11, ha = 'right')

    # plt.plot(npma(np.arange(nbins),10),npma(np.interp(mean_dta, blim, [0, nbins]),10), color = (1., .05, .05))
    plt.subplots_adjust(left=.1, bottom=0.1, top=0.92, right=0.98)
    plt.gca().yaxis.set_label_coords( -0.105, .99)
    plt.gca().xaxis.set_label_coords(.75, 0.125)
    plt.xlim([0., nbins+ .1])
    plt.ylim([0., nbins+ .1])
    plt.show()
    
    
def replot_scatter_density_dta(h, region_name, plim, blim, vvmax = 5e2):
    if vvmax == 'auto':
        vvmax = 0.5*np.max(h)
    nbins = np.shape(h)[0]
    plt.figure(figsize = (4.5, 3.))
    # plt.imshow(h, norm=colors.LogNorm(vmin=1, vmax=np.max(h)), cmap='YlGnBu')#Blues, YlGnBu, GnBu
    plt.imshow(h, norm=colors.LogNorm(vmin=1, vmax=vvmax), cmap='YlGnBu')#Blues, YlGnBu, GnBu
    plt.colorbar(label='day count per pixel')
    
    # Set Major Ticks
    logxtval = np.arange(np.log10(plim[0]), np.log10(plim[1]), 1.)
    text_xticks = np.hstack([0, 10.**logxtval, plim[1]-1])
    target_xticks = np.log10(text_xticks+1)
    tickloc_bindim = np.interp(target_xticks, [np.log10(plim[0]), np.log10(plim[1])], [0, np.shape(h)[0]])
    # plt.gca().set_xticks(np.log10(target_xticks+1))
    plt.gca().set_xticks(tickloc_bindim)
    plt.gca().set_xticklabels(text_xticks.astype(int).astype(str))
    # plt.xlabel('Daily Snowfall (kg m$^{-2}$ d$^{-1}$)')
    plt.xlabel('SF (kg m$^{-2}$ d$^{-1}$)')
    # plt.ylabel(u'Temperature Anomaly (°C)')
    plt.ylabel(u'$\Delta$T (°C)', rotation = 0)
    plt.xlim([0., nbins+ .1])
    ytickname = np.arange(-20., 36., 10.)
    ytickloc = np.interp(ytickname, blim, [0, nbins])
    plt.gca().set_yticks(ytickloc)
    plt.gca().set_yticklabels(ytickname.astype(str))
    plt.gca().invert_yaxis()
    plt.annotate(region_name, xy = (10, nbins-5), fontsize=16)
    
    #  Set minorticks
    logxtval2 = np.arange(0., 1.1, 1.)
    temp = np.vstack([2*10.**logxtval2, 3*10.**logxtval2, 4*10.**logxtval2, 5*10.**logxtval2, 
                      6*10.**logxtval2, 7*10.**logxtval2, 8*10.**logxtval2, 9*10.**logxtval2])
    tt=(np.sort(np.reshape(temp, (np.size(temp),)))+1)
    target_minxticks = np.log10(tt[tt<plim[1]])
    mintickloc_bindim = np.interp(target_minxticks, [np.log10(plim[0]), np.log10(plim[1])], [0, np.shape(h)[0]])
    plt.gca().xaxis.set_minor_locator(tck.FixedLocator(mintickloc_bindim))
    plt.gca().spines['top'].set_visible(False)
    plt.gca().spines['right'].set_visible(False)
    # draw mean
    yhvals = np.interp(np.arange(nbins)+0.5, [0, nbins], blim)
    mean_dta = np.zeros(nbins)
    for sf in range(nbins):
        sfbin_dta = np.multiply(h[:,sf], yhvals)
        mean_dta[sf] = np.sum(sfbin_dta)/np.sum(h[:,sf])
    # plt.plot(np.arange(nbins),np.interp(mean_dta, blim, [0, nbins]), color = (1., .65, .65), lw = .4)
    plt.plot(np.arange(nbins),np.interp(mean_dta, blim, [0, nbins]), color = (1., .05, .05))
    # plt.plot(npma(np.arange(nbins),10),npma(np.interp(mean_dta, blim, [0, nbins]),10), color = (1., .05, .05))
    plt.subplots_adjust(left=-0.4, bottom=0.1, top=0.92, right=0.98)
    plt.gca().yaxis.set_label_coords( -0.105, .99)
    plt.gca().xaxis.set_label_coords(.725, 0.125)
    plt.show()
# =============================================================================
#%% Load Data, define regions, select sites
# =============================================================================
# format
# ==================================
size = 15
params = {'legend.fontsize': 'medium',
          'axes.labelsize': size,
          'axes.titlesize': size,
          'xtick.labelsize': size * 0.9,
          'ytick.labelsize': size * 0.9,
          'axes.titlepad': 25}
plt.rcParams.update(params)
# ==================================

# === read MAR === #
# ================ #

# MAR grid
lon2d = xr.open_mfdataset('./data/MARv312/grid/LON_MARv312-albCor105-spinup6.nc4')['LON']
lat2d = xr.open_mfdataset('./data/MARv312/grid/LAT_MARv312-albCor105-spinup6.nc4')['LAT']
sh = xr.open_mfdataset('./data/MARv312/grid/SH_MARv312-albCor105-spinup6.nc4')['SH']
# 2 m air temperature (°C)
ta = xr.open_mfdataset('./data/MARv312/TT_MARv312-albCor105-spinup6_*_daily.nc4')['TT'].isel(atmlay=0)
# snowfall (mm day-1)
pr = xr.open_mfdataset('./data/MARv312/MBsf_MARv312-albCor105-spinup6_*_daily.nc4')['MBsf']
# space axes
xx, yy = ta.x, ta.y


parts = {'Antarctica': (sh > 0.),
         'Ocean Domain': (sh <= 0.),
         'West': ((sh > 0.) & ((lon2d > -180.) & (lon2d < -40.))),
         'East Low': (lon2d > -40.) & ((sh > 0.) & (sh <= 2000.)),
         'East High': (lon2d > -40.) & (sh >2000.)}

def find_mar_gridpoint(req_lat, req_lon, xmar, ymar, latmar, lonmar):
    get1 = range(len(xmar))
    get2 = range(len(ymar))
    getx, gety = np.meshgrid(get1, get2)
    distance = np.sqrt((latmar-req_lat)**2 + (lonmar-req_lon)**2)
    closest_grid = distance == np.min(distance)
    xp = getx[closest_grid]
    yp = gety[closest_grid]
    return xp[0], yp[0]

txtcoords = np.linspace(-180, 180.*5./6., 12)
xcoords = np.zeros(len(txtcoords)).astype(int)
ycoords = np.zeros(len(txtcoords)).astype(int)
for i in range(len(txtcoords)):
    tx, ty = find_mar_gridpoint(-60., txtcoords[i], xx, yy, lat2d, lon2d)
    xcoords[i], ycoords[i] = find_mar_gridpoint(lat2d.isel(x=tx, y=ty)-3, lon2d.isel(x=tx, y=ty), xx, yy, lat2d, lon2d)

# sitecoords = {'Dronning Maud Land': (-75., -0.33),
#               'High plateau': (-79.85, 90.02),
#               'Law Dome': (-66.73, 112.75),
#               'Dome C': (-75.35, 123.33),
#               'Ross Ice Shelf': (-79.85, 179.83),
#               'Ocean': (-70.26, -149.99),
#               'WAIS Divide': (-79.68, -112.33),
#               'Gomez': (-74.06, -70.86),
#               'West Peninsula': (-70.01, -66.73),
#               'East Peninsula': (-69.88, -60.02)}

# sitemar = {}
# for site in sitecoords:
#     sitemar[site] = find_mar_gridpoint(sitecoords[site][0], sitecoords[site][1], xx, yy, lat2d, lon2d)

# Reload manually saved:
sitemar = {'Dronning Maud Land': (86, 120),
            'High plateau': (118, 73),
            'Law Dome': (153, 45),
            'Dome C': (124, 48),
            'Ross Ice Shelf': (87, 41),
            'Ocean': (55, 19),
            'WAIS Divide': (57, 61),
            'Gomez': (39, 89),
            'West Peninsula': (28, 98),
            'East Peninsula': (31, 105)}

# =============================================================================
#%% Figure 1a
# =============================================================================
def show_parts(regions, cmap='plasma', extend='neither', label=''):
    xmin, xmax = -2800., 2800.
    ymin, ymax = -2500., 2500.
    levels = np.arange(0, len(regions), 1)
    plt.figure(figsize=(5.5*3/4, 3.))#plt.figure(figsize = (4.5, 3.))
    plt.clf()
    plt.subplots_adjust(left=0.02, bottom=0.02, top=0.98, right=0.98)
    plt.setp(plt.gca().get_xaxis(), visible=False)#Hide bottom x-axis
    plt.setp(plt.gca().get_yaxis(), visible=False)#Hide y-axis
    i=0
    mapr = 0* parts['Antarctica'].values.astype(int)
    for region_name in regions:
        mapr = mapr + i*parts[region_name].values.astype(int)
        i+=1

    plt.contourf(xx, yy, mapr, levels=levels, cmap=cmap, extend=extend, zorder = i+2)
    plt.colorbar(label=label, pad=0.02)
    plt.xlim(xmin, xmax)
    plt.ylim(ymin, ymax)
    plt.contour(xx, yy, lat2d, np.linspace(-80, -50, 4), colors='k', linewidths=.6, linestyles='dotted', zorder = i+3)
    plt.contour(xx, yy, lon2d.where((lat2d >= -88) & (lon2d>-179)), np.linspace(-180, 180.*5./6., 12), colors='k', linewidths=.7, linestyles='dotted', zorder = i+2)
    plt.contour(xx, yy, sh, [0.], linewidths=1.2, colors='k', zorder = i+3)
    plt.contour(xx, yy, sh, [100., 1000., 2000., 3000.], linewidths=.6, colors='k', zorder = i+4)
    coordannot = [u'180°', u'150°W', u'120°W', u'90°W', u'60°W', u'30°W', u'0°', u'30°E', u'60°E', u'90°E', u'120°E', u'150°E']
    # txtcoords = np.linspace(-180, 180.*5./6., 12)
    # xcoords = np.zeros(len(txtcoords)).astype(int)
    # ycoords = np.zeros(len(txtcoords)).astype(int)
    for i in range(len(coordannot)):
        plt.annotate(coordannot[i], xy=(xx.isel(x =xcoords[i]),  yy.isel(y=ycoords[i])), zorder = 15, color = 'w')

show_parts(parts)



# =============================================================================
#%% Figure maps: 2, 5, 6, 7
# =============================================================================
# === define standard map plot === #
# ================================ #

vmin, vmax, delta = -10, 10, 1.
cmap = 'RdBu_r'

# =============================================================================
# # Figure 2: precipitation-weighted temperature difference
# =============================================================================
# ==========================================
ta_prwt = ta.weighted(pr)
ta_tmean = ta.mean('time')
ta_prwt_tmean = ta_prwt.mean('time')
delta_ta_prwt = ta_prwt_tmean - ta_tmean
label = '$\Delta$T: snowfall weighted \n temperature difference (°C)'
shade(delta_ta_prwt, vmin, vmax, delta, cmap=cmap, label=label)
overlay_sites()

# =============================================================================
# # Figure 4 map of seasonal cycle reduction 
# =============================================================================
doy_index = ta.time.dt.dayofyear#create an index for all years based on repetition of doy

# ================================
# 1: compute normal  seasonal cycle amplitudes
# 2: compute weighted seasonal cycle amplitudes
ta_doy = ta.groupby("time.dayofyear").mean('time').sel(dayofyear=doy_index).rolling(time=30, center=True).mean()
ta_prwt_doy = ((ta * pr).groupby("time.dayofyear").sum('time') / pr.groupby("time.dayofyear").sum('time')).sel(dayofyear=doy_index).rolling(time=30, center=True).mean()

# np.shape(ta_doy.values)
cnt_ar = ta_doy.values[365:731, :, :]
cnt_sf = ta_prwt_doy.values[365:731, :, :]

# 3: ratio
cnt_reduction = (np.std(cnt_sf, axis=0)/np.std(cnt_ar, axis=0)-1)*100
# 4: plot using shade

# std_ratio = ta_prwt_interannualstd/ta_interannualstd - 1
label = 'relative change of \n seasonal amplitude (%)'
shade(cnt_reduction, -60., 60., 10, cmap=cmap, label=label)

# =============================================================================
# # Figure 5a: seasonal effect only
# =============================================================================
# ============================
ta_seasonal = ta.groupby("time.dayofyear").mean('time')#average on doy
ta_seasmean = ta_seasonal.sel(dayofyear=doy_index)
ta_seasmean_rolling = ta_seasmean.rolling(time=30, center=True).mean()

# precipitation-weighted seasonal temperature
ta_seasmean_prwt = ta_seasmean_rolling.weighted(pr)
# time mean variables
ta_seasmean_tmean = ta_seasmean_rolling.mean('time')
ta_seasmean_prwt_tmean = ta_seasmean_prwt.mean('time')
delta_ta_seasmean = ta_seasmean_prwt_tmean - ta_seasmean_tmean
label = 'Snowfall-weighted \ntemperature difference (°C)'
shade(delta_ta_seasmean, vmin, vmax, delta, cmap=cmap, label=label)

# =============================================================================
# # Figure 5b: precipitation effect only
# =================================
delta_ta = ta - ta_seasmean_rolling
delta_ta_prwt = delta_ta.weighted(pr)
delta_ta_prwt_tmean = delta_ta_prwt.mean('time')
# plot
# label = 'Precip: (T $-$ Tseas)_Pwt (°C)'
label = 'Anomaly to seasonal \n temperature (°C)'
shade(delta_ta_prwt_tmean, vmin, vmax, delta, cmap=cmap, label=label)


# =============================================================================
# # Figure 6: Difference in Inter-Annual variability between ta_prwt and ta
# # ========================================================================
ta_interannualstd = ta.resample(time='1Y').mean('time').std('time')
ta_prwt_interannualstd = ((ta*pr).resample(time='1Y').sum('time')*(1./pr.resample(time='1Y').sum('time'))).std('time')

# 6a
label = 'interannual \nTemperature St.Dev. (°C)'
shade(ta_interannualstd, 0., 3., .2, cmap='Reds', label=label)

# 6b
label = 'interannual snowfall-weighted \nTemperature St.Dev. (°C)'
shade(ta_prwt_interannualstd, 0., 3., .2, cmap='Reds', label=label)

# 6c
std_diff = ta_prwt_interannualstd - ta_interannualstd
label = 'difference in interannual St.Dev. (°C)'
shade(std_diff, -2., 2., .2, cmap=cmap, label=label)


# 6d
std_ratio = ta_prwt_interannualstd/ta_interannualstd - 1
label = 'relative change of \n interannual st. dev. (%)'
shade(100.*std_ratio, 20.*vmin, 20.*vmax, 20.*delta, cmap=cmap, label=label)

# =============================================================================
#%% Stats calculs for in text description
# =============================================================================

subsample = parts['Antarctica']

ta_prwt = ta.weighted(pr)
# time mean variables
ta_tmean = ta.mean('time')
ta_prwt_tmean = ta_prwt.mean('time')
delta_ta_prwt = ta_prwt_tmean - ta_tmean

# 'average tsfwt'
print('Delta T Temperature Difference (°C) Antarctic Average: '+str(np.nanmean(delta_ta_prwt.where(subsample, drop = True).values)))

# 'Seasonal effect on \n temperature anomaly (°C)'
print('Seasonal effect on \n temperature anomaly (°C) Antarctic Average: '+str(np.nanmean(delta_ta_seasmean.where(subsample, drop = True).values)))

# label = 'Anomaly to seasonal \n temperature (°C)'
print('non-seasonal \n temperature (°C) Antarctic Average: '+str(np.nanmean(delta_ta_prwt_tmean.where(subsample, drop = True).values)))

# label = 'Anomaly to seasonal \n temperature (°C)'
print('relative change of \n interannual st. dev. (%) Antarctic Average: '+str(np.nanmean((100.*std_ratio).where(subsample, drop = True).values)))

# =============================================================================
#%% values of seasonal amplitude
# =============================================================================
ta_doy = ta.groupby("time.dayofyear").mean('time').sel(dayofyear=doy_index).rolling(time=30, center=True).mean()
pr_monthly = pr.resample(time='1M').sum('time').groupby('time.month').mean('time')

region = parts['Antarctica']
ta_doy_region = ta_doy.where(region).mean(('x', 'y'))
pr_mon_region = pr_monthly.where(region).mean(('x', 'y'))
# ta_seas_doy_region = ta_seas_doy.where(region).mean(('x', 'y'))
ta_prwt_doy = ((ta * pr).groupby("time.dayofyear").sum('time') / pr.groupby("time.dayofyear").sum('time')).sel(dayofyear=doy_index).rolling(time=30, center=True).mean()

ta_prwt_doy_region = ta_prwt_doy.where(region).mean(('x', 'y'))
cnt_ar = ta_doy_region.values[365:731]
cnt_sf = ta_prwt_doy_region.values[365:731]

print('CNTar seasonal amplitude : '+str(2*np.std(cnt_ar)))
print('CNTsfwt seasonal amplitude : '+str(2*np.std(cnt_sf)))
print('seasonal amplitude by std reduction = '+ str(((np.std(cnt_sf)/np.std(cnt_ar)-1)*100)))

print('CNTar seasonal range : '+str(cnt_ar.max()-cnt_ar.min()))
print('CNTsfwt seasonal range : '+str(cnt_sf.max()-cnt_sf.min()))
print('seasonal amplitude by range reduction = '+ str((((cnt_sf.max()-cnt_sf.min())/(cnt_ar.max()-cnt_ar.min())-1)*100)))



# =============================================================================
#%% Stats calculs for Table 2
# =============================================================================
def temperature_at_site(site):
    print('Temperature anomalies for snowfall >0 at site: '+ site)
    subsample = sitemar[site]
    minipr = pr.isel(x=subsample[0], y=subsample[1])
    tpr1 = delta_ta.isel(x=subsample[0], y=subsample[1]).where(minipr>0., drop = True).weighted(minipr).mean('time')
    t_ave = np.nanmean(tpr1)
    print('sfwt DT = '+str(t_ave))

for site in sitemar:
    temperature_at_site(site)
    
def temperature_at_sf1(site):
    print('Temperature anomalies for snowfall >1 at site: '+ site)
    subsample = sitemar[site]
    minipr = pr.isel(x=subsample[0], y=subsample[1])
    tpr1 = delta_ta.isel(x=subsample[0], y=subsample[1]).where(minipr>1., drop = True).values
    t_ave = np.nanmean(tpr1)
    t_q95 = np.nanquantile(tpr1, .95)
    t_q84 = np.nanquantile(tpr1, .84)
    t_q50 = np.nanquantile(tpr1, .50)
    t_q16 = np.nanquantile(tpr1, .16)
    t_q05 = np.nanquantile(tpr1, .05)
    print('average DT = '+str(t_ave))
    print('DT quantile 95 = '+str(t_q95))
    print('DT quantile 84  = '+str(t_q84))
    print('DT quantile 50 = '+str(t_q50))
    print('DT quantile 16 = '+str(t_q16))
    print('DT quantile 05  = '+str(t_q05))
    
    
for site in sitemar:
    temperature_at_sf1(site)
    
    

# =============================================================================
#%% trends regressions vs log10(1+SF) for Figure 2
# =============================================================================

sfthreshold = .05
for site in sitemar:
    subsample = sitemar[site]
    print('DT - SF corr for: '+ site)
    sf = pr.isel(x=subsample[0], y=subsample[1])
    x = np.log10(1. + sf.where(sf>sfthreshold, drop = True).values)
    y = delta_ta.isel(x=subsample[0], y=subsample[1]).where(sf>sfthreshold, drop = True).values
    c = ta.isel(x=subsample[0], y=subsample[1]).where(sf>sfthreshold, drop = True).values
    nonan = np.logical_and(np.isfinite(x), np.isfinite(y))
    scatter_corr(x[nonan], y[nonan], c[nonan], w = sf.where(sf>sfthreshold, drop = True).values[nonan])
    plt.xlabel('Log$_{10}$(1+Snowfall)')
    plt.ylabel('Temperature Anomaly (°C)')
    plt.title(site)

# =============================================================================

# =============================================================================
#%% Figure 4 and 5: Seasonal cycles with error shadings
# =============================================================================
doy_index = ta.time.dt.dayofyear#create an index for all years based on repetition of doy

ta_doy = ta.groupby("time.dayofyear").mean('time').sel(dayofyear=doy_index).rolling(time=30, center=True).mean()
ta_doystd = ta.groupby("time.dayofyear").std('time').sel(dayofyear=doy_index).rolling(time=30, center=True).mean()
ta_prwt_doy = ((ta * pr).groupby("time.dayofyear").sum('time') / pr.groupby("time.dayofyear").sum('time')).sel(dayofyear=doy_index).rolling(time=30, center=True).mean()
ta_prwt_doystd = np.sqrt((((ta-ta_doy)**2 * pr).groupby("time.dayofyear").sum('time') 
                          / pr.groupby("time.dayofyear").sum('time'))
                         ).sel(dayofyear=doy_index).rolling(time=30, center=True).mean()

pr_monthly = pr.resample(time='1M').sum('time').groupby('time.month').mean('time')
pr_monthlystd = pr.resample(time='1M').sum('time').groupby('time.month').std('time')


dim = np.array([31., 29., 31., 30., 31., 30., 31., 31., 30., 31., 30., 31.])# Days In Month
mlist = np.array(['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D'])
# plot
# def plot_seasonalcycle_region(region_name):
#     region = parts[region_name]
#     ta_doy_region = ta_doy.where(region).mean(('x', 'y'))
#     pr_mon_region = pr_monthly.where(region).mean(('x', 'y'))
#     ta_doy_regionstd = ta_doystd.where(region).mean(('x', 'y'))
#     pr_mon_regionstd = pr_monthlystd.where(region).mean(('x', 'y'))
#     # ta_seas_doy_region = ta_seas_doy.where(region).mean(('x', 'y'))
#     ta_prwt_doy_region = ta_prwt_doy.where(region).mean(('x', 'y'))
#     ta_prwt_doy_regionstd = ta_prwt_doystd.where(region).mean(('x', 'y'))
#     fb, axl = create_naxis(2, 0.)
    
#     # figure a: temperature
#     axl[0].plot(ta_prwt_doy_region.time.dayofyear.values[365:731], ta_prwt_doy_region[365:731], color='#FE6100', lw = 2., label = '$\overline{T_{w}}$', zorder = 6)
#     axl[0].fill_between(ta_prwt_doy_region.time.dayofyear.values[365:731], 
#                         y1 = (ta_prwt_doy_region - ta_prwt_doy_regionstd)[365:731], 
#                         y2 = (ta_prwt_doy_region + ta_prwt_doy_regionstd)[365:731], 
#                         color='#FE6100', alpha = .3, lw = 2., label = '$\overline{T_{w}}$', zorder = 3)
#     axl[0].plot(ta_doy_region.time.dayofyear.values[365:731], ta_doy_region.values[365:731], color=(.2, .2, .2), lw = 2., label = '$\overline{T}$', zorder = 4)
#     axl[0].fill_between(ta_doy_region.time.dayofyear.values[365:731], 
#                         y1 = (ta_doy_region - ta_doy_regionstd)[365:731], 
#                         y2 = (ta_doy_region + ta_doy_regionstd)[365:731], 
#                         color=(.2, .2, .2), alpha = .3, lw = 2., label = '$\overline{T_{w}}$', zorder = 2)
#     axl[0].set_ylabel(u'Temperature (°C)')
#     axl[0].grid(color=(.9, .9, .9), zorder = 1)
#     axl[0].legend()
#     axl[0].set_title(region_name)

#     # Figure b: bars of monthly precip
#     axl[1].bar(np.cumsum(dim), pr_mon_region.values, 
#             width = -1*dim, align='edge', color='#5072CC', edgecolor = 'w', zorder = 2)
#     axl[1].errorbar(np.cumsum(dim), pr_mon_region.values, yerr=pr_mon_regionstd.values, 
#             capsize = 5.,color='#202d51', zorder = 3)
#     axl[1].set_xlabel('day of year')
#     axl[1].set_ylabel('monthly snowfall \n (kg m$^{-2}$ month$^{-1}$)')
#     axl[1].grid(color=(.9, .9, .9), zorder = 1)
#     plt.setp(fb.get_xaxis(), visible=True)
#     fb.grid(color = ((.9, .9, .9)))
#     for mi in range(12):
#         axl[1].annotate(mlist[mi], xy=(np.cumsum(dim)[mi]-15., 3.), color = 'w', size=15)
#     axl[1].set_xlim([0., 366.])

# plot_seasonalcycle_region('Antarctica')

# =============================================================================
# hard plot antarctica
# =============================================================================

region = parts['Antarctica']
tyr = np.arange(1, 367, 1, int)
ta_doy_region = ta_doy.where(region).mean(('x', 'y')).values[365:731]
pr_mon_region = pr_monthly.where(region).mean(('x', 'y'))
ta_doy_regionstd = ta_doystd.where(region).mean(('x', 'y')).values[365:731]
pr_mon_regionstd = pr_monthlystd.where(region).mean(('x', 'y'))
# ta_seas_doy_region = ta_seas_doy.where(region).mean(('x', 'y'))
ta_prwt_doy_region = ta_prwt_doy.where(region).mean(('x', 'y')).values[365:731]
ta_prwt_doy_regionstd = ta_prwt_doystd.where(region).mean(('x', 'y')).values[365:731]
fb, axl = create_naxis(2, 0.)

# figure a: temperature
axl[0].plot(tyr, ta_prwt_doy_region, color='#FE6100', lw = 2., label = '$\overline{T_{w}}$', zorder = 6)
axl[0].fill_between(tyr, 
                    y1 = (ta_prwt_doy_region - ta_prwt_doy_regionstd), 
                    y2 = (ta_prwt_doy_region + ta_prwt_doy_regionstd), 
                    color='#FE6100', alpha = .3, lw = 2., edgecolor = 'none', zorder = 3)
axl[0].plot(tyr, ta_doy_region, color=(.2, .2, .2), lw = 2., label = '$\overline{T}$', zorder = 4)
axl[0].fill_between(tyr, 
                    y1 = (ta_doy_region - ta_doy_regionstd), 
                    y2 = (ta_doy_region + ta_doy_regionstd), 
                    color=(.2, .2, .2), alpha = .3, lw = 2., edgecolor = 'none', zorder = 2)
axl[0].set_ylabel(u'Temperature (°C)')
axl[0].grid(color=(.9, .9, .9), zorder = 1)
axl[0].legend()
axl[0].set_title('Antarctica')
# Figure b: bars of monthly precip
axl[1].bar(np.cumsum(dim), pr_mon_region.values, 
        width = -1*dim, align='edge', color='#5072CC', edgecolor = 'w', zorder = 2)
axl[1].errorbar(np.cumsum(dim)-16, pr_mon_region.values, yerr=pr_mon_regionstd.values, 
        capsize = 5., ls = 'none', color='#202d51', zorder = 3)
axl[1].set_xlabel('day of year')
axl[1].set_ylabel('monthly snowfall \n (kg m$^{-2}$ month$^{-1}$)')
axl[1].grid(color=(.9, .9, .9), zorder = 1)
plt.setp(fb.get_xaxis(), visible=True)
fb.grid(color = ((.9, .9, .9)))
for mi in range(12):
    axl[1].annotate(mlist[mi], xy=(np.cumsum(dim)[mi]-16., 3.), color = 'w', size=15)
axl[1].set_xlim([0., 366.])


def encart_seasonalcycle_site(site):
    subsample = sitemar[site]
    ta_doy_region = ta_doy.isel(x=subsample[0], y=subsample[1]).values[365:731]
    pr_mon_region = pr_monthly.isel(x=subsample[0], y=subsample[1])
    ta_prwt_doy_region = ta_prwt_doy.isel(x=subsample[0], y=subsample[1]).values[365:731]
    ta_doy_regionstd = ta_doystd.isel(x=subsample[0], y=subsample[1]).values[365:731]
    pr_mon_regionstd = pr_monthlystd.isel(x=subsample[0], y=subsample[1])
    ta_prwt_doy_regionstd = ta_prwt_doystd.isel(x=subsample[0], y=subsample[1]).values[365:731]
    
    fb, axl = create_naxis(2, 0.)
    plt.gcf().set_size_inches(3.5, 3.)
    # figure a: temperature
    axl[0].plot(tyr, ta_prwt_doy_region, color='#FE6100', lw = 2., label = '$\overline{T_{w}}$', zorder = 3)
    axl[0].fill_between(tyr, 
                        y1 = (ta_prwt_doy_region - ta_prwt_doy_regionstd), 
                        y2 = (ta_prwt_doy_region + ta_prwt_doy_regionstd), 
                        color='#FE6100', alpha = .3, lw = 2., edgecolor = 'none', zorder = 3)
    axl[0].plot(tyr, ta_doy_region, color=(.2, .2, .2), lw = 2., label = '$\overline{T}$', zorder = 2)
    axl[0].fill_between(tyr, 
                        y1 = (ta_doy_region - ta_doy_regionstd), 
                        y2 = (ta_doy_region + ta_doy_regionstd), 
                        color=(.2, .2, .2), alpha = .3, lw = 2., edgecolor = 'none', zorder = 2)
    axl[0].set_ylabel(u'T (°C)', rotation = 0)
    axl[0].grid(color=(.9, .9, .9), zorder = 1)
    l = axl[0].legend(loc = 'lower right', frameon=False)
    l.get_texts()[0].set_color('#FE6100')
    # axl[0].annotate
    axl[0].set_title(site, y=1.0, pad=-15)

    # Figure b: bars of monthly precip
    axl[1].bar(np.cumsum(dim), pr_mon_region.values, 
            width = -1*dim, align='edge', color='#5072CC', edgecolor = 'w', zorder = 2)
    axl[1].errorbar(np.cumsum(dim)-16, pr_mon_region.values, yerr=pr_mon_regionstd.values, 
            capsize = 5., ls = 'none', color='#202d51', zorder = 3)
    axl[1].set_xlabel('day of year')
    axl[1].set_ylabel(u'SF (kg m$^{-2}$ month$^{-1}$)')
    axl[1].grid(color=(.9, .9, .9), zorder = 1)
    plt.setp(fb.get_xaxis(), visible=True)
    fb.grid(color = ((.9, .9, .9)))
    for mi in range(12):
        axl[1].annotate(mlist[mi], xy=(np.cumsum(dim)[mi]-24., 3.), color = 'w')
    axl[1].set_xlim([0., 366.])
    axl[0].yaxis.set_label_coords( -0.02, 1.02)
    # axl[1].yaxis.set_label_coords( .91, 1.31)
    # plt.gca().xaxis.set_label_coords(.75, 0.125)

for site in sitemar:
    print('Histogram for: '+ site)
    encart_seasonalcycle_site(site)
encart_seasonalcycle_site('Ross Ice Shelf')
# =============================================================================
#%% Figure 1 and 2 Scatter Plots: define and save histograms 
# =============================================================================

from fast_histogram import histogram2d
# import matplotlib.ticker as tck

# =============================================================================
# define histogram parts of antarctica (Figure 1)
# =============================================================================
blim = [-25., 35.]
plim = [1., 101.]

def plot_scatter_density_dta(region_name, nbins):
    subsample = parts[region_name]

    all_delta_ta = delta_ta.where(subsample, drop = True).stack(z=('x', 'y', 'time'))
    logscale_pp = np.log10(1+pr.where(subsample, drop = True).stack(z=('x', 'y', 'time')))
    bounds = [blim, np.log10(plim)]
    h = histogram2d(all_delta_ta, logscale_pp, range=bounds, bins=nbins)
    return h

    
for region_name in parts:
    print('Scatter plot for region: '+ region_name)
    hist_data = plot_scatter_density_dta(region_name, nbins=200)
    with open('hist_data_'+region_name, 'wb') as ph:  # Python 3: open(..., 'rb')
        pickle.dump(hist_data, ph)
    
# =============================================================================
# define histogram sites (Figure 2)
# =============================================================================
blim = [-25., 35.]
plim = [1., 81.]

def def_scatter_density_dta(site, nbins):
    subsample = sitemar[site]
    all_delta_ta = delta_ta.isel(x=subsample[0], y=subsample[1])
    logscale_pp = np.log10(1+pr.isel(x=subsample[0], y=subsample[1]))
    bounds = [blim, np.log10(plim)]
    h = histogram2d(all_delta_ta, logscale_pp, range=bounds, bins=nbins)
    return h

for site in sitemar:
    print('Histogram for: '+ site)
    hist_data = def_scatter_density_dta(site, nbins=80)
    with open('hist_data_'+site, 'wb') as ph:  # Python 3: open(..., 'rb')
        pickle.dump(hist_data, ph)
    print('(saved)')


# =============================================================================
#%% Figure 2 scatter heatmaps (fixed max value to 5e2) from a reload of saved histograms
# =============================================================================

blim = [-25., 35.]
plim = [1., 81.]

xdx = np.linspace(0,  np.log10(plim[-1]), 100)#x is log10(1+SF)
# computed for SF > 0.05 with snowfall weighting
trends_per_site = {'Dronning Maud Land': (24.72, 1.29, 0.48),
                    'High plateau': (127.34, -3.62, 0.50),
                    'Law Dome': (11.35, -4.03, 0.50),
                    'Dome C': (69.67, -0.97, 0.49),
                    'Ross Ice Shelf': (26.02, -2.68, 0.39),
                    'Ocean': (15.14, -4.94, 0.27),
                    'WAIS Divide': (22.27, -2.81, 0.26),
                    'Gomez': (12.44, -6.51, 0.34),
                    'West Peninsula': (10.67, -8.98, 0.27),
                    'East Peninsula': (8.91, -4.27, 0.08)}
# source: trends version log10(1+SF)
    
for site in sitemar:
    with open('hist_data_'+site, 'rb') as ph:  # Python 3: open(..., 'rb')
        hist_data = pickle.load(ph)
    print('Scatter plot for: '+ site)
    replot_encart_scatter_density_dta(hist_data, site, plim, blim)


#%% Get figure 2 colorbar (fixed max value to 5e2)
for site in sitemar:
    with open('hist_data_'+site, 'rb') as ph:  # Python 3: open(..., 'rb')
        hist_data = pickle.load(ph)
    print('Scatter plot for: '+ site)
    replot_scatter_density_dta(hist_data, site, plim, blim)
    
#%% Figure 1 scatter heatmaps (free max value) from a reload of saved histograms


blim = [-25., 35.]
plim = [1., 101.]

for site in parts:
    with open('hist_data_'+site, 'rb') as ph:  # Python 3: open(..., 'rb')
        hist_data = pickle.load(ph)
    print('Scatter plot for: '+ site)
    replot_scatter_density_dta(hist_data, site, plim, blim, vvmax = 'auto')
    



# =============================================================================
#%% Figure 3 Tw-T scatter density
# =============================================================================

ta_interannual = ta.resample(time='1Y').mean('time')
ta_prwt_interannual = ((ta*pr).resample(time='1Y').sum('time')*(1./pr.resample(time='1Y').sum('time')))


from fast_histogram import histogram2d
# import matplotlib.ticker as tck

blim = [-60., 0.]
plim = [-60., 0.]

def plot_scatter_density_ttw(region_name, nbins):
    subsample = parts[region_name]

    vplot = ta_prwt_interannual.where(subsample, drop = True).stack(z=('x', 'y', 'time'))
    hplot = ta_interannual.where(subsample, drop = True).stack(z=('x', 'y', 'time'))
    bounds = [blim, plim]
    h = histogram2d(vplot, hplot, range=bounds, bins=nbins)
    return h

    
for region_name in parts:
    print('Scatter plot for region: '+ region_name)
    hist_data = plot_scatter_density_ttw(region_name, nbins=200)
    with open('T-Tw_hist_data_'+region_name, 'wb') as ph:  # Python 3: open(..., 'rb')
        pickle.dump(hist_data, ph)



for region_name in parts:
    with open('T-Tw_hist_data_'+region_name, 'rb') as ph:  # Python 3: open(..., 'rb')
        h = pickle.load(ph)
    print('Scatter plot for: '+ region_name)
    # replot_scatter_density_dta(hist_data, site, plim, blim, vvmax = 'auto')
    # region_name, plim, blim, vvmax = 5e2
    vvmax = 0.5*np.max(h)
    nbins = np.shape(h)[0]
    plt.figure(figsize = (4.5, 3.))
    # plt.imshow(h, norm=colors.LogNorm(vmin=1, vmax=np.max(h)), cmap='YlGnBu')#Blues, YlGnBu, GnBu
    plt.imshow(h, norm=colors.LogNorm(vmin=1, vmax=vvmax), cmap='YlGnBu', zorder = 3)#Blues, YlGnBu, GnBu
    plt.colorbar(label='year count per pixel')
    
    # # Set Major Ticks
    # plt.ylabel(u'Temperature Anomaly (°C)')
    plt.ylabel(u'annual snowfall-weighted temperature (°C)')
    plt.xlabel(u'annual temperature (°C)')
    plt.xlim([0., nbins+ .1])
    ytickname = np.arange(-60., 5., 10.)
    ytickloc = np.interp(ytickname, blim, [0, nbins])
    plt.gca().set_yticks(ytickloc)
    plt.gca().set_yticklabels(ytickname.astype(str))
    plt.gca().set_xticks(ytickloc)
    plt.gca().set_xticklabels(ytickname.astype(str))
    plt.gca().invert_yaxis()
    plt.annotate(region_name, xy = (10, nbins-5), fontsize=16)
    
    plt.gca().spines['top'].set_visible(False)
    plt.gca().spines['right'].set_visible(False)
    # draw mean
    yhvals = np.interp(np.arange(nbins)+0.5, [0, nbins], blim)
    mean_dta = np.zeros(nbins)
    for sf in range(nbins):
        sfbin_dta = np.multiply(h[:,sf], yhvals)
        mean_dta[sf] = np.sum(sfbin_dta)/np.sum(h[:,sf])
    plt.plot(np.arange(nbins),np.interp(mean_dta, blim, [0, nbins]), color = (1., .05, .05),zorder = 5)
    plt.subplots_adjust(left=-0.4, bottom=0.1, top=0.92, right=0.98)
    plt.plot(np.arange(nbins),np.arange(nbins), ls = '--',  color = (.05, .05, .05),zorder = 4)
    # plt.gca().yaxis.set_label_coords( -0.105, .99)
    # plt.gca().xaxis.set_label_coords(.725, 0.125)
    plt.grid(zorder = 2, color = (.9, .9, .9))
    plt.show()


# =============================================================================
# %% Reply to Review#3: Sime 2008 filtered temperature biases
# =============================================================================


vmin, vmax, delta = -10, 10, 1.
cmap = 'RdBu_r'

import xfilter
ta_tmean = ta.mean('time')

# no decompose (Fig 2 central)
ta_prwt = ta.weighted(pr)
ta_prwt_tmean = ta_prwt.mean('time')
delta_ta_prwt = ta_prwt_tmean - ta_tmean
label = '$\Delta$T: snowfall weighted \n temperature difference (°C)'
shade(delta_ta_prwt, vmin, vmax, delta, cmap=cmap, label=label)
overlay_sites()

# lowpass
ta_LP = (ta - ta_tmean).filter.lowpass(1/750, dim='time', order=4)
pr_LP = pr.filter.lowpass(1/750, dim='time', order=4)

# ta_LP_prwt = ta_LP.weighted(pr_LP) NG
ta_LP_prwt = (ta_LP*pr_LP).sum('time')/pr.sum('time')
# delta_ta_prwt_LP = ta_LP_prwt
label = '$\Delta$T: snowfall weighted \n temperature difference for interannual scale (>375 days) (°C)'
shade(ta_LP_prwt, vmin, vmax, delta, cmap=cmap, label=label)
overlay_sites()
del ta_LP_prwt, pr_LP

# Bandpass
ta_BP = (ta - ta_tmean).filter.bandpass([ 1/750,1/61], dim='time', order=4)
pr_BP = pr.filter.bandpass([ 1/750,1/61], dim='time', order=4)
ta_BP_prwt = (ta_BP*pr_BP).sum('time')/pr.sum('time')
# delta_ta_prwt_BP = ta_BP_prwt
label = '$\Delta$T: snowfall weighted \n temperature difference for seasonal scale (60~375 days) (°C)'
shade(ta_BP_prwt, vmin, vmax, delta, cmap=cmap, label=label)
overlay_sites()
del ta_BP_prwt, pr_BP

# highpass
ta_HP = (ta - ta_tmean).filter.highpass(1/60, dim='time', order=4)
pr_HP = pr.filter.highpass(1/60, dim='time', order=4)

ta_HP_prwt = (ta_HP*pr_HP).sum('time')/pr.sum('time')
# delta_ta_prwt_HP = ta_HP_prwt
label = '$\Delta$T: snowfall weighted \n temperature difference for synoptic scale (<60 days) (°C)'
shade(ta_HP_prwt, vmin, vmax, delta, cmap=cmap, label=label)
overlay_sites()
del ta_HP_prwt, pr_HP

# test plot
loc = sitemar['Dome C']
fig, axes = plt.subplots(1, 1)
ax = axes
(ta - ta_tmean).isel(x=loc[0], y=loc[1]).plot(label='raw', color='k', ax=ax)
ta_HP.isel(x=loc[0], y=loc[1]).plot(label='highpass', color='orange', ax=ax)
ta_BP.isel(x=loc[0], y=loc[1]).plot(label='bandpass', color='b', ax=ax)
ta_LP.isel(x=loc[0], y=loc[1]).plot(label='lowpass', color='green', ax=ax)
plt.legend()
del ta_BP, ta_HP, ta_LP


# advertise
fig, axes = plt.subplots(1, 1)
ax = axes
xfilter.highpass_response(1/60, order=4).plot(label='60-day highpass', ax=ax)
ax.axvline(1/60, ls='--', color='C0')
xfilter.bandpass_response([1/750, 1/60], order=4).plot(label='750-to-60-day bandpass', ax=ax)
ax.axvspan(1/750, 1/60, alpha=0.3, color='C1')
xfilter.lowpass_response(1/750, order=4).plot(label='750-day lowpass', ax=ax)
ax.axvline(1/750, ls='--', color='C2')
ax.legend()
plt.gca().set_xticklabels(1/plt.gca().get_xticks())
plt.xlabel('period (days)')
plt.tight_layout()

# # test plot
# loc = sitemar['Dome C']
# fig, axes = plt.subplots(2, 1)
# ax = axes[0]
# ta.isel(x=loc[0], y=loc[1]).plot(label='raw', color='k', ax=ax)
# ta_BP.isel(x=loc[0], y=loc[1]).plot(label='bp', color='b', ax=ax)
# ax = axes[1]
# pr_BP.isel(x=loc[0], y=loc[1]).plot(label='bp', color='b', ax=ax)

# ta_prwt = ta.weighted(pr)
# ta_prwt_tmean = ta_prwt.mean('time')
# delta_ta_prwt = ta_prwt_tmean - ta_tmean
# label = '$\Delta$T: snowfall weighted \n temperature difference (°C)'
# shade(delta_ta_prwt, vmin, vmax, delta, cmap=cmap, label=label)
# overlay_sites()





#%% Appendix figure 4 micro-rain radar


# =============================================================================
# # reload MAR @ DDU
# =============================================================================

lon2d = xr.open_mfdataset('./data/MARv312/grid/LON_MARv312-albCor105-spinup6.nc4')['LON']
lat2d = xr.open_mfdataset('./data/MARv312/grid/LAT_MARv312-albCor105-spinup6.nc4')['LAT']
sh = xr.open_mfdataset('./data/MARv312/grid/SH_MARv312-albCor105-spinup6.nc4')['SH']
ta = xr.open_mfdataset('./data/MARv312/TT_MARv312-albCor105-spinup6_*_daily.nc4')['TT'].isel(atmlay=0)
pr_ = xr.open_mfdataset('./data/MARv312/MBsf_MARv312-albCor105-spinup6_*_daily.nc4')
xx, yy = ta.x, ta.y

def find_mar_gridpoint(req_lat, req_lon, xmar, ymar, latmar, lonmar):
    get1 = range(len(xmar))
    get2 = range(len(ymar))
    getx, gety = np.meshgrid(get1, get2)
    distance = np.sqrt((latmar-req_lat)**2 + (lonmar-req_lon)**2)
    closest_grid = distance == np.min(distance)
    xp = getx[closest_grid]
    yp = gety[closest_grid]
    return xp[0], yp[0]

site_ddu = find_mar_gridpoint(-(66+39/60.+46/3600.), 140+4/3600., xx, yy, lat2d, lon2d)

pr_ddu = pr_.isel(x=site_ddu[0], y=site_ddu[1]).sel(time=slice("2014-11-01", "2016-12-31"))
# pr_ddu_short = pr_ddu.sel(time=slice("2014-11-01", "2016-12-31"))

# =============================================================================
# load ddu precips
# =============================================================================
'''
data from 
Grazioli, J., Genthon, C., Boudevillain, B., Duran-Alarcon, C., Del Guasta, M., 
Madeleine, J.-B., and Berne, A.: Measurements of precipitation in Dumont d'Urville, 
Adélie Land, East Antarctica, The Cryosphere, 11, 1797–1811, , 2017
https://doi.org/10.5194/tc-11-1797-2017
'''
import cftime
import pandas as pd
import datetime
ddumrr = xr.open_mfdataset('C:\\Python\\LSCE_WE\\DDU Micro Rain Radar\\precipitation_mrr_height_levels_localrelation.nc')
ddumrr['time'] = xr.coding.times.decode_cf_datetime(ddumrr.TimeS, units='seconds since 1970-01-01', calendar='gregorian')

ddumrr = ddumrr.assign_coords(time=ddumrr.time)
ddumrr_daily = ddumrr.groupby("time.date").sum()

pr_ddu_resettime = pr_ddu.groupby("time.date").sum()

# =============================================================================
# combine by date
# =============================================================================
compare = xr.combine_by_coords([ddumrr_daily.sel(height=ddumrr_daily['height'][0]).Precip_J60, pr_ddu_resettime['MBsf']])


# Time series plot
plt.figure()
plt.plot(compare.date, compare.Precip_J60, 
         ls = 'none', marker = '.', zorder = 3, label = 'Micro-Rain Radar (Grazioli et al. 2017)')
plt.plot(compare.date, compare.MBsf, 
         ls = '-', zorder = 2, label = 'MAR (this study)')
plt.legend()
plt.ylabel('Daily Snowfall (kg m$^{-2}$ d$^{-1}$)')

# scatter plot logscale
plt.figure()
plt.plot(np.log10(1+compare.MBsf), np.log10(1+compare.Precip_J60), 
         ls = 'none', marker = '+', zorder = 3)

nonan = np.logical_and(np.isfinite(compare.MBsf.values), np.isfinite(compare.Precip_J60.values))
r, p_val = stats.pearsonr(np.log10(1+compare.MBsf.values[nonan]), np.log10(1+compare.Precip_J60.values[nonan]))

plt.ylabel('Daily Snowfall (kg m$^{-2}$ d$^{-1}$)\nMicro-Rain Radar (Grazioli et al. 2017)')
plt.xlabel('Daily Snowfall (kg m$^{-2}$ d$^{-1}$)\nMAR (this study)')
xline = np.array([0, np.max(compare.MBsf)])
plt.plot(np.log10(1+xline), np.log10(1+xline), 
         ls = '--', color = 'k',  zorder = 2)

logxtval = np.arange(np.log10(1+xline[0]), 1+np.log10(1+xline[1]), 1.)
text_xticks = np.hstack([0, 10.**logxtval])
target_xticks = np.log10(text_xticks+1)
# ytickname = np.arange(np.log(1+xline[0]), 1+np.log(1+xline[1]), 10.)
# ytickloc = np.interp(ytickname, blim, [0, nbins])
plt.gca().set_yticks(target_xticks)
plt.gca().set_yticklabels(text_xticks.astype(str))
plt.gca().set_xticks(target_xticks)
plt.gca().set_xticklabels(text_xticks.astype(str))
plt.grid(color=(.9, .9, .9), zorder = 1)
# plt.gca().set_xticks(ytickloc)
# plt.gca().set_xticklabels(ytickname.astype(str))
# =============================================================================
# count
# =============================================================================
cluster1 = np.logical_and(compare.MBsf.values <5e-3, compare.Precip_J60.values<5e-3 )# both no precip
cluster2 = np.logical_and(compare.MBsf.values >5e-3, compare.Precip_J60.values<5e-3 )# model precip, no obs precip
cluster3 = np.logical_and(compare.MBsf.values <5e-3, compare.Precip_J60.values>5e-3 )# no model precip, obs precip
cluster4 = np.logical_and(compare.MBsf.values >5e-3, compare.Precip_J60.values>5e-3 )# both precip
labels = 'both no SF', 'only model SF', 'only obs. SF', 'both SF'
sizes = [cluster1.sum(), cluster2.sum(), cluster3.sum(), cluster4.sum()]


import matplotlib.cm as cm
colmap = cm.get_cmap('Blues')
palette = [None]*6
for i in range(6):
    palette[i] = colmap(float(i)/(6-1.))

fig, ax = plt.subplots()
ax.pie(sizes, labels=labels, autopct='%1.1f%%', colors = palette[1:-1])



# =============================================================================
#%% Reply to Review#2: Southern Annular Mode
# =============================================================================


# =============================================================================
# LOAD SAM
# =============================================================================
import pandas as pd
from datetime import timedelta, datetime
from dateutil.relativedelta import relativedelta
'''
SOURCE for SAM monthly index: 
https://legacy.bas.ac.uk/met/gjma/sam.html [accessed 2023-10-13]
'''

samise = pd.read_csv("C:\\Python\\LSCE_WE\\SAM\\SAMi.csv").set_index('Unnamed: 0').stack()
start_date = datetime(1957, 2, 1)
end_date = datetime(2023, 10, 1)
 
# Initialize an empty list
date_list = []
 
# Loop through the range of dates and append to the list
while start_date <= end_date:
    date_list.append(start_date+ relativedelta(days=-1)) 
    start_date += relativedelta(months=1)

# mon_list = OrderedDict(((start + timedelta(_)).strftime("%Y-%m"), None) for _ in range((end - start).days)).keys()
samise.index=pd.to_datetime(date_list)

sami =pd.DataFrame(samise)
sami.index = pd.to_datetime(samise.index)
temp=np.zeros(len(sami))
temp[0]=1957.
for i in range(len(sami)-1):
    temp[i+1]= temp[i]+1./12.
sami['decimalyear']=np.round(temp+1./24,3)
xrsam = xr.Dataset(data_vars=dict(sami=(["time"], sami.iloc[:,0])), 
                   coords=dict(time=sami.index))

# =============================================================================
# plot: for each 10 sites, the monthly T anomaly (y-axis) as fct of SAM index (x-axis)
# =============================================================================
doy_index = ta.time.dt.dayofyear#create an index for all years based on repetition of doy
ta_seasonal = ta.groupby("time.dayofyear").mean('time')#average on doy

ta_seasmean = ta_seasonal.sel(dayofyear=doy_index)
ta_seasmean_rolling = ta_seasmean.rolling(time=30, center=True).mean()
delta_ta = ta - ta_seasmean_rolling

def scatter_corr1(x, y, marker_pt = '+', color_pt = 'k', color_ln = 'b', linestyle = '-'):
    plt.figure()
    nonan = np.logical_and(np.isfinite(x), np.isfinite(y))
    x, y, = x[nonan], y[nonan]
    a, b = do_odr_corr(x, y, precision=3)
    plt.close(plt.gcf())
    plt.figure()
    somin, somax = x.min(), x.max()
    plt.plot(x, y, ls = 'none', marker = marker_pt, color = color_pt)
    plt.plot(np.array([somin, somax]), a*np.array([somin, somax])+ b, color = color_ln, ls = linestyle)
    plt.annotate('y = '+"%.2f" % a+' * x + '+"%.2f" % b, xy=(0.02, 0.95), xycoords='axes fraction')
    # r_sq = np.corrcoef(y, a*so_surface.loc[:, x]+b)[0,1]**2
    r, p_val = stats.pearsonr(y, a*x+b)
    plt.annotate('r$^{2}$ = '+ "%.3f" % r**2, xy=(0.02, 0.88), xycoords='axes fraction')
    if p_val >= 0.01:
        plt.annotate('P$_{value}$ = '+"%.3f" % p_val, xy=(0.02, 0.81), xycoords='axes fraction')
    else:
        plt.annotate('P$_{value}$ < 0.01', xy=(0.02, 0.81), xycoords='axes fraction')

for site in sitemar:
    print(site)
    subsample = sitemar[site]
    # print('DT - SF corr for: '+ site)
    mon_dta = delta_ta.isel(x=subsample[0], y=subsample[1]).resample(time='1M').mean()
    merged = xr.combine_by_coords([xrsam, mon_dta])
    # plt.figure()
    scatter_corr1(merged.sami.values, merged.TT.values)
    plt.xlabel('SAM Marshall index (monthly)')
    plt.ylabel('Monthly Temperature Anomaly (°C)')
    plt.title(site)


# =============================================================================
# plot: for each 10 sites, the monthly DeltaT (y-axis) as fct of SAM index (x-axis)
# =============================================================================

ta_mon = ta.resample(time='1M').mean('time')
ta_prwt_mon = ((ta*pr).resample(time='1M').sum('time')*(1./pr.resample(time='1M').sum('time')))

prw_ta_minus_ta = (ta_prwt_mon - ta_mon).rename('DeltaT')

for site in sitemar:
    print(site)
    subsample = sitemar[site]
    # print('DT - SF corr for: '+ site)
    mon_dta = prw_ta_minus_ta.isel(x=subsample[0], y=subsample[1])
    merged = xr.combine_by_coords([xrsam, mon_dta])
    # plt.figure()
    scatter_corr1(merged.sami.values, merged.DeltaT.values)
    plt.xlabel('SAM Marshall index (monthly)')
    plt.ylabel('Monthly $T_{w}-T$  (°C)')
    plt.title(site)

corrmap = xr.corr(prw_ta_minus_ta, xrsam.sami,dim='time')
shade(corrmap, -1, 1, .1, cmap='RdBu_r', extend='neither', label='SAM-$\Delta{}T$ correlation')
overlay_sites()

# =============================================================================
# plot: for each 10 sites, the yearly DeltaT (y-axis) as fct of SAM index (x-axis)
# =============================================================================

ta_mon = ta.resample(time='1Y').mean('time')
ta_prwt_mon = ((ta*pr).resample(time='1Y').sum('time')*(1./pr.resample(time='1Y').sum('time')))

prw_ta_minus_ta = (ta_prwt_mon - ta_mon).rename('DeltaT')

for site in sitemar:
    print(site)
    subsample = sitemar[site]
    # print('DT - SF corr for: '+ site)
    mon_dta = prw_ta_minus_ta.isel(x=subsample[0], y=subsample[1])
    merged = xr.combine_by_coords([xrsam.resample(time='1Y').mean('time'), mon_dta])
    # plt.figure()
    scatter_corr1(merged.sami.values, merged.DeltaT.values)
    plt.xlabel('SAM Marshall index (monthly)')
    plt.ylabel('Monthly $T_{w}-T$  (°C)')
    plt.title(site)


